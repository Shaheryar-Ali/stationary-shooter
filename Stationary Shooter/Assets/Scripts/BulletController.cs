﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public Vector3 shootDirection;
    public float movespeed;
    public float damage = 10;
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {
        transform.position += shootDirection * movespeed; // * Time.deltaTime;
    }

    public void SetDirection(Vector3 direction)
    {
        shootDirection = direction;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("enemy"))
        {
            //Code on enemy collision
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("obstacle"))
        {
            //Bullet bounce
            Destroy(gameObject);
        }

    }

    public float GetDamage()
    {
        return damage;
    }
}
