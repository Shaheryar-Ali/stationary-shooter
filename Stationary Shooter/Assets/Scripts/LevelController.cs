﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public int maxEnemyInLevel;
    private int enemiesSpawned;
    private int enemiesKilled;
    
    void Awake()
    {
        resetLevel();
    }

    public bool AllowSpawn()
    {
        return enemiesSpawned < maxEnemyInLevel;
    }

    public void EnemySpawnAdd()
    {
        enemiesSpawned++;
    }

    public void EnemyKilled()
    {
        enemiesKilled++;
    }

    public void resetLevel()
    {
        enemiesSpawned = 0;
        enemiesKilled = 0;
    }


 

}
