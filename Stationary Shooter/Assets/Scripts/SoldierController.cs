﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierController : MonoBehaviour
{

    public Animator animator;
    int counter;
    public Vector3 shootDirection;
    public int shootDelay;
    public GameObject gunShotEffect;

    // Start is called before the first frame update
    void Start()
    {
        counter = 0;
        shootDirection = Vector3.zero;
        gunShotEffect.GetComponent<ParticleSystem>().Stop();

    }

    private void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            animator.SetBool("isShooting", false);
            gunShotEffect.GetComponent<ParticleSystem>().Stop();
            counter = 0;
        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetMouseButton(0))
        {
            gunShotEffect.GetComponent<ParticleSystem>().Play();
            animator.SetBool("isShooting", true);
            //Code to look at mouse
            Vector3 v3T = Input.mousePosition;
            v3T.z = Mathf.Abs(Camera.main.transform.position.y - transform.position.y);
            v3T = Camera.main.ScreenToWorldPoint(v3T);
            v3T -= transform.position;

            shootDirection = v3T;

            v3T = v3T * 10000.0f + transform.position;
            transform.LookAt(v3T);
            if(counter == 0)
            {
                GetComponent<ShootBullets>().Shoot();
            }
            counter++;
            if(counter > shootDelay)
            {
                counter = 0;
            }
        }
        
    }

    private void OnApplicationQuit()
    {
        gunShotEffect.GetComponent<ParticleSystem>().Stop();
    }
}
