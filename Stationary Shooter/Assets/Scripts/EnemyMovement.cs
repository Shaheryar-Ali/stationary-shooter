﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    public float health = 50;
    [SerializeField]
    Transform player;
//    Transform[] path;       //Vector to be used to give path other than player
//    int currentPoint;       //Current point in path

    NavMeshAgent navMeshAgent;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();

        player = GameObject.FindGameObjectWithTag("Player").transform;
        

        if(navMeshAgent == null)
        {
            Debug.LogError("No NavMeshAgent component");
        }

//        currentPoint = 0;

        SetDestination();
    }

    // Update is called once per frame
    void Update()
    {
        if(health <= 0)
        {
            Destroy(gameObject);
            GetComponentInParent<EnemySpawner>().ReduceEnemyCount();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            health = health - collision.gameObject.GetComponent<BulletController>().GetDamage();
        }

        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Game Over");
        }
    }

    private void SetDestination()
    {
        navMeshAgent.SetDestination(player.position);
    }
     
}
