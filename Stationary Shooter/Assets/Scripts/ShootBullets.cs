﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBullets : MonoBehaviour
{
    public GameObject bullet;
    public GameObject muzzle;


    public void Shoot()
    {
        GameObject firedBullet = Instantiate(bullet, muzzle.transform);
        Vector3 shootDirection = GetComponent<SoldierController>().shootDirection;
        shootDirection = shootDirection.normalized;
        firedBullet.GetComponent<BulletController>().SetDirection(shootDirection);
        firedBullet.transform.parent = null;
    }
}
