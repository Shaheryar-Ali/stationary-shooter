﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotalEnemyManager : MonoBehaviour
{
    public int maxEnemyCount;       //Total Enemies allowed in a level;
    private int currentEnemyCount;   //Current number of enemies;
    
    void Awake()
    {
        currentEnemyCount = 0;
    }

    // Update is called once per frame


    public bool AllowSpawn()
    {
        return currentEnemyCount < maxEnemyCount;
    }

    public void IncreaseCurrentEnemyCount()
    {
        ++currentEnemyCount;
    }

    public void DecreaseCurrentEnemyCount()
    {
        --currentEnemyCount;
    }


}
