﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    float timer;
    public float spawnDelay;
    public GameObject enemyPrefab;
    int enemyCount;
    public int maxEnemyCount;

    // Start is called before the first frame update
    void Start()
    {
        timer = Mathf.Sqrt(spawnDelay);
        enemyCount = 0;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer = timer + Time.deltaTime;

        if(timer >= spawnDelay)
        {
            if(enemyCount < maxEnemyCount && GetComponentInParent<TotalEnemyManager>().AllowSpawn() && transform.parent.parent.GetComponent<LevelController>().AllowSpawn())
            {
                //Spawn enemy code
                GameObject newEnemy = Instantiate(enemyPrefab);
                newEnemy.transform.SetParent(gameObject.transform);
                newEnemy.transform.position = new Vector3(transform.position.x, newEnemy.transform.position.y, transform.position.z);
                enemyCount++;
                GetComponentInParent<TotalEnemyManager>().IncreaseCurrentEnemyCount();
                transform.parent.parent.GetComponent<LevelController>().EnemySpawnAdd();
            }

            timer = 0;
        }
        
    }


    public void ReduceEnemyCount()
    {
        enemyCount--;
        GetComponentInParent<TotalEnemyManager>().DecreaseCurrentEnemyCount();
        transform.parent.parent.GetComponent<LevelController>().EnemyKilled();
    }
}
